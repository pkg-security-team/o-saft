#! /usr/bin/make -rRf
#?
#? DESCRIPTION
#?      For more details please see
#?          ../Makefile  Makefile  Makefile.help  Makefile.pod
#?      make help.test.exit
#?
#? VERSION
#?      @(#) Makefile.exit 1.30 22/11/13 21:36:02
#?
#? AUTHOR
#?      18-apr-18 Achim Hoffmann
#?
# -----------------------------------------------------------------------------

HELP-help.test.exit = targets for testing '$(SRC.pl)' --exit= option

_SID.exit          := 1.30

_MYSELF.exit       := t/Makefile.exit
ALL.includes       += $(_MYSELF.exit)
ALL.inc.type       += exit
ALL.help.tests     += help.test.exit

first-exit-target-is-default: help.test.exit

ifeq (,$(_SID.test))
    -include t/Makefile
endif

TEST.exit.hosts     = localhost
ifdef TEST.hosts
    TEST.exit.hosts = $(TEST.hosts)
endif

help.test.exit:       HELP_TYPE = exit
help.test.exit-v:     HELP_TYPE = exit
help.test.exit-vv:    HELP_TYPE = exit

HELP-_exit1         = ___________________________________ testing --exit* option _
HELP-test.exit      = test --exit=* options
HELP-test.exit.log  = same as test.exit but store output in '$(TEST.logdir)/'

HELP.exit           = # no special documentation yet
HELP.test.exit.all  = # no special documentation yet

# all known --exit=  are shown with:  o-saft.pl --norc --help=exit

# SEE Make:target name
# SEE Make:target name prefix

LIST.o-saft.pl--exit := \
	--exit=invalid_label_to_show_failed-status \
	--exit=BEGIN0  --exit=BEGIN1   --exit=INIT0    --exit=CONF0 --exit=CONF1 \
	--exit=INIT1   --exit=ARGS     --exit=MAIN \
	--exit=HOST0   --exit=HOST1    --exit=HOST2    --exit=HOST3 --exit=HOST4 \
	--exit=HOST5   --exit=HOST6    --exit=HOST8    --exit=HOST9 --exit=END

# all targets are generated, see Makefile.gen; only one program: SRC.pl

testarg-exit-o-saft.pl_%:   EXE.pl      = ../$(SRC.pl)
testarg-exit-o-saft.pl_%:   TEST.init   = --trace-CLI +cn $(TEST.host)
#testarg-exit-o-saft.pl_--exit-HOST0:   TEST.init   = --trace-CLI +cn $(TEST.host)

ifndef exit-macros-generated
    # must use GEN.targets instead of GEN.targets-args to avoid setting TEST.init
    $(eval $(call GEN.targets,testarg,exit,-$(SRC.pl),$(SRC.pl),LIST.o-saft.pl--exit,TEST.args,_TEST.dumm))
endif

# some special targets
testarg-exit-o-saft.pl_--exit-WARN:     TEST.args  += --exit=WARN   +force-warning
#testarg-exit-o-saft.pl_--exit-HOST7:    TEST.args  += --exit=HOST7

ALL.test.exit.log   = $(ALL.test.exit:%=%.log)

test.exit.log-compare:  TEST.target_prefix  = testarg-exit
test.exit.log-move:     TEST.target_prefix  = testarg-exit
test.exit.log:          TEST.target_prefix  = testarg-exit

test.exit:          $(ALL.test.exit)
test.exit.log:      $(ALL.test.exit.log)
