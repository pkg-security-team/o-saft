o-saft (22.11.22-1) unstable; urgency=medium

  * New upstream version 22.11.22
  * Bump Standards-Version to 4.6.1
  * d/p/install_only_important_files.patch: Update patch

 -- Samuel Henrique <samueloph@debian.org>  Sun, 18 Dec 2022 15:32:13 +0000

o-saft (22.02.22-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.0, no changes needed.

  [ Samuel Henrique ]
  * New upstream version 22.02.22
  * Commit all 22.02.22-1 changes at once :(
    - The whole packaging of o-saft had to be refactored as I didn't have
      enough experience to do it right the first time. This is an improved
      version of the packaging with hopefully no regressions.
      I don't have the time to split the changes into different commits or
      changelog entries (and our time is valuable), so here are the big
      changes:
    - Stop installing upstream's contrib files, there's a lot of things
      there and I don't think there's anyone using it.
    - Stop relying on Makefile for packaging, we only need to call the
      install script. This allows me to remove most of the previous
      patches. Two new patches were needed for this, but they will be
      much more simple to maintain.
    - Add a dependency on ca-certificates

 -- Samuel Henrique <samueloph@debian.org>  Thu, 10 Mar 2022 23:42:15 +0000

o-saft (19.01.19-3) unstable; urgency=medium

  * d/{control|copyright}: Update upstream URL (closes: #968985)

 -- Samuel Henrique <samueloph@debian.org>  Thu, 11 Feb 2021 00:04:34 +0000

o-saft (19.01.19-2) unstable; urgency=medium

  [ Samuel Henrique ]
  * Bump DH to 13
  * Bump Standards-Version to 4.5.1
  * Add salsa-ci.yml
  * Configure git-buildpackage for Debian
  * d/control: Add Rules-Requires-Root: no

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.
  * Update standards version to 4.5.0, no changes needed.

 -- Samuel Henrique <samueloph@debian.org>  Fri, 29 Jan 2021 19:48:57 +0000

o-saft (19.01.19-1) unstable; urgency=medium

  * New upstream version 19.01.19
  * Bump DH level to 12
  * Bump Standards-Version to 4.3.0
  * Refresh patches

 -- Samuel Henrique <samueloph@debian.org>  Tue, 29 Jan 2019 19:36:25 +0000

o-saft (18.11.18-1) unstable; urgency=medium

  * New upstream version 18.11.18
  * d/copyright: use https
  * d/o-saft.install: reflect filename change
  * d/p/tests: update patch
  * d/p/makefile: update patch

 -- Samuel Henrique <samueloph@debian.org>  Thu, 06 Dec 2018 23:16:38 +0000

o-saft (18.07.18-1) unstable; urgency=medium

  * New upstream version 18.07.18
  * d/control: add openssl dependency
  * d/o-saft.install: reflect new upstream location of o-saft_bench
  * d/t/control: better test command and mark as superficial

 -- Samuel Henrique <samueloph@debian.org>  Wed, 31 Oct 2018 00:17:53 -0300

o-saft (17.09.17-2) unstable; urgency=high

  * Revert "symlink on /usr/bin instead of using helper script"
    (closes: #911402)

 -- Samuel Henrique <samueloph@debian.org>  Sat, 20 Oct 2018 01:19:31 -0300

o-saft (17.09.17-1) unstable; urgency=medium

  * Initial Debian release (closes: #905998)
  * Bump DH to 11
  * Bump Standards-Version to 4.2.0
  * Bump watch to v4
  * Move maintenance to Security Tools Packaging Team
  * Don't ship perlcriticrc, .pod file and unneeded pdf
  * Symlink on /usr/bin instead of using helper script
  * d/control: set priority to optional, was extra
  * d/copyright: update file
  * d/o-saft.lintian-overrides:
     - add lintian-override for dockerfile
  * d/rules:
    - don't make Dockerfile executable
    - remove template comments and trailing whitespace
  * wrap-and-sort -a

 -- Samuel Henrique <samueloph@debian.org>  Mon, 13 Aug 2018 12:07:25 -0300

o-saft (17.09.17-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@freexian.com>  Mon, 09 Oct 2017 07:03:12 +0200

o-saft (15.11.15-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Use debhelper 9
  * Update debian files
  * Install the file for bash completion

 -- Sophie Brun <sophie@freexian.com>  Tue, 15 Dec 2015 15:27:29 +0100

o-saft (14.1.4-0kali1) kali; urgency=low

  * Initial release (Closes: 0000827)

 -- Devon Kearns <dookie@kali.org>  Wed, 12 Feb 2014 12:35:06 -0700
